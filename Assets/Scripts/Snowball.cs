using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Snowball : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        Touched();
    }
    private void OnTriggerEnter(Collider other)
    {
        Touched();
    }

    private void Touched()
    {
        Destroy(gameObject);
    }
}

